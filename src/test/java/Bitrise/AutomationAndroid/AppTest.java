package Bitrise.AutomationAndroid;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;


/**
 * Unit test for simple App.
 */
public class AppTest {
	public static AppiumDriver<MobileElement> driver;
	public DesiredCapabilities capabilities;

	@BeforeTest
	public void start() throws MalformedURLException {

		capabilities = new DesiredCapabilities();        	
    	capabilities.setCapability("automationName", "appium");   
    	capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Android device");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.example.googlemap");

        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.example.googlemap.MainActivity");
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
       
        System.out.println("ANDROID IS RUNNING");

	}

	@Test
	public void launchApp() {
		driver.findElement(By.xpath("//XCUIElementTypeOther[1]//XCUIElementTypeSearchField[1]")).sendKeys("LONDON");

	}
}
